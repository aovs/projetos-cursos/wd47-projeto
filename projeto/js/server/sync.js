export async function getInstrucoes() {
    const resposta = await fetch('http://wd47-ceep.herokuapp.com/get-instrucoes.php');
    const dadosCarregados = await resposta.json();
    const mensagens = dadosCarregados.instrucoes;

    return mensagens;
}

export async function salvarCartoes(listaCartoes) {
    const infoUsuario = {
        usuario: 'seuemail@dominio.com.br',
        cartoes: listaCartoes
    };

    let url = 'http://wd47-ceep.herokuapp.com/salvar-cartoes.php';
    const respostaServidor = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-type': 'application/json'
        },
        body: JSON.stringify(infoUsuario)
    });

    const dadosRetornados = await respostaServidor.json();
    
    if (dadosRetornados.quantidade == 1) {
        return '01 cartão salvo com sucesso!';
    }
    else {
        return dadosRetornados.quantidade + ' cartões salvos com sucesso!';
    }
}

export async function getCartoesSalvos()
{
    let usuario = 'seuemail@dominio.com.br';
    let url = 'http://wd47-ceep.herokuapp.com/get-cartoes.php?usuario=' + usuario;

    const resposta = await fetch(url);
    const dadosCartoes = await resposta.json();

    return dadosCartoes.cartoes ?? [];
}