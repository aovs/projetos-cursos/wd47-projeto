let db;
const subscribers = [];

const requestDb = indexedDB.open('db_ceep_backup', 1);
requestDb.addEventListener('success', () => {
    db = requestDb.result;
    carregarCartoes();
});

requestDb.addEventListener('upgradeneeded', () => {
    db = requestDb.result;
    db.createObjectStore('store_cartoes', { keyPath: 'id', autoIncrement: true });
});

export function salvarCartoesStore(listaCartoes)
{
    return new Promise(async function(resolve, reject) {
        await excluirCartoes();
        const tx = db.transaction('store_cartoes', 'readwrite');

        for (let cartao of listaCartoes)
        {
            const dadosCartao = { conteudo: cartao.conteudo, cor: cartao.cor };
            tx.objectStore('store_cartoes').add(dadosCartao);
        }

        tx.oncomplete = () => resolve('Cartões salvos com sucesso na base de dados local!');
        tx.onerror = () => reject('Erro ao salvar dados na base de dados local!');
    });
}

export function IDBSubscribeCartoes(funcaoCallback)
{
    subscribers.push(funcaoCallback);
}

function carregarCartoes()
{
    const tx = db.transaction('store_cartoes');
    const request = tx.objectStore('store_cartoes').getAll();
    request.onsuccess = () => {
        const listaCartoes = request.result ?? [];
        subscribers.forEach(fn => fn(listaCartoes));
    }
}

export function excluirCartoes()
{
    return new Promise(function(resolve, reject) {
        const tx = db.transaction('store_cartoes', 'readwrite');
        tx.objectStore('store_cartoes').clear();
        tx.oncomplete = () => resolve('Cartões locais excluídos com sucesso!');
        tx.onerror = () => reject('Erro ao excluir cartões da base de dados local!');
    });
}